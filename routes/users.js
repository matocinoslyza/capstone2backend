//[SECTION] Dependencies and Modules
const exp = require("express"); 
const controller = require('./../controllers/users.js');
const auth = require("../auth") 

// destructure verify from auth
const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
const route = exp.Router(); 

// [SECTION] Routes-[POST]
route.post('/register', (req, res) => {
  let userDetails = req.body; 
  controller.registerUser(userDetails).then(outcome => {
     res.send({message: "outcome"});
    // return true;
  });
});

// route.post("/register", controller.registerUser);

// LOGIN

route.post("/login", controller.loginUser);

// GET USER DETAILS
//[SECTION] Routes-[GET]

// note: routes that have verify as a middleware would require us to pass a token from postman
route.get("/getUserDetails", verify, controller.getUserDetails)


// GET ALL USER DETAILS
//[SECTION] Routes-[GET]
route.get("/getAllUsers", verify, verifyAdmin, controller.getAllUsers)

// [SECTION] Routes-[PUT]
route.put('/:id', verify, verifyAdmin, (req, res) => {
    let id = req.params.id; 
    let details = req.body;
    let firstName = details.fname; 
    let lastName = details.lname;
    let email = details.email;
    let password = details.password;        
    if (firstName  !== '' && lastName !== '' && email !== '' && password !== '') {
      controller.updateUser(id, details).then(outcome => {
          res.send(outcome); 
      });      
    } else {
      res.send('Incorrect Input, Make sure details are complete');
    }
}); 

// [SECTION] Routes-[PUT] [SET A USER AS ADMIN]
route.put('/:id/setAsAdmin', verify, verifyAdmin, (req, res) => {
    let userId = req.params.id;
    controller.setAsAdmin(userId).then(resultOfTheFunction => {
      res.send(resultOfTheFunction);
    });
  });

// [SECTION] Routes-[PUT] [SET ADMIN AS USER]
route.put('/:id/setAsUser', verify, verifyAdmin, (req, res) => {
    let userId = req.params.id;
    controller.setAsUser(userId).then(resultOfTheFunction => {
      res.send(resultOfTheFunction);
    });
  });

// [SECTION] Routes-[DELETE]
route.delete('/:id', verify, verifyAdmin, (req, res) => {
    let id = req.params.id; 
    controller.deleteUser(id).then(outcome => {
       res.send(outcome);
    });
 });


// [SECTION] Expose Route System
module.exports = route;
