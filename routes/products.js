//[SECTION] Dependencies and Modules
const exp = require("express");
const controller = require('../controllers/products');
const auth = require('../auth');


// destructure verify from auth
const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
const route = exp.Router(); 

//[SECTION] [POST] Routes
 route.post('/create', verify, verifyAdmin, (req, res) => { 
     let data = req.body; 
     controller.createProduct(data).then(outcome => {
          res.send(outcome); 
     });
 });

//[SECTION] [GET] Routes
//[SECTION] [GET ALL PRODUCT] Routes [ADMIN]
 route.get('/all', (req, res) => {
    controller.getAllProduct().then(outcome => {
      res.send(outcome);
    });
 });

 //[SECTION] [GET SINGLE PRODUCT] Routes
 route.get('/:id', (req, res) => {
    let productId = req.params.id; 
    controller.getProduct(productId).then(result => { 
        res.send(result);
    }); 
 }); 

 //[SECTION] [GET] Routes [GET ALL ACTIVE]
 route.get('/', (req, res) => {
    controller.getAllActiveProduct().then(outcome => {
       res.send(outcome); 
    });
 });

//[SECTION] [PUT] Routes
route.put('/:id', verify, verifyAdmin, (req, res) => {
    let id = req.params.id; 
    let details = req.body;
    let cName = details.name; 
    let cDesc = details.description;
    let cCost = details.price;       
    if (cName  !== '' && cDesc !== '' && cCost !== '') {
      controller.updateProduct(id, details).then(outcome => {
          res.send(outcome); 
      });      
    } else {
      res.send({message: 'Incorrect Input, Make sure details are complete'});
    }
}); 

// route for deactivate
route.put('/:id/archive', verify, verifyAdmin, (req, res) => {
    let productId = req.params.id; 
    controller.deactivateProduct(productId).then(resultOfTheFunction => {
        res.send(resultOfTheFunction);
    });
});
  // route for reactivate
route.put('/:id/reactivate', verify, verifyAdmin, (req, res) => {
  let productId = req.params.id; 
  controller.reactivateProduct(productId).then(resultOfTheFunction => {
     res.send(resultOfTheFunction);
  });
});

//[SECTION] [DEL] Routes 
route.delete('/:id', verify, verifyAdmin, (req, res) => {
   let id = req.params.id; 
   controller.deleteProduct(id).then(outcome => {
      res.send(outcome);
   });
});

//[SECTION] Export Route System
module.exports = route; 
