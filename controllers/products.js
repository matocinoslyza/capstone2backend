// [SECTION] Dependencies and Modules
const Product = require('../models/Product');

// [SECTION] Functionality [CREATE]
module.exports.createProduct = (info) => {
    let cName = info.name;
    let cDesc = info.description;
    let cCost = info.price;

    let newProduct = new Product({
        name: cName,
        description: cDesc,
        price:  cCost
    })

    return newProduct.save().then((savedProduct, error) => {
        if (error) {
            return false;
        }else{
    
            return savedProduct;
        }
    })
}
// [SECTION] Functionality [RETRIEVE]
    module.exports.getAllProduct = () => {
        return Product.find({}).then(result => {
            return result;
        });
    };
    // Retrieve a single product
    module.exports.getProduct = (id) => {
            return Product.findById(id).then(resultOfQuery => {
                return resultOfQuery;
            });
    };
    module.exports.getAllActiveProduct = () => {
        return Product.find({isActive: true}).then(resultOftheQuery => {
            return resultOftheQuery;
        });
    };

// [SECTION] Functionality [UPDATE]
    module.exports.updateProduct  = (id, details) => {
        let cName = details.name;
        let cDesc = details.description;
        let cCost = details.price;

        let updatedProduct = {
            name: cName, 
            description: cDesc,
            price: cCost
        }

        return Product.findByIdAndUpdate(id, details).then((productUpdated, err) => {
            if (err) {
                return false;
            }else{
                return true;
            }
        }); 
    };
// Deactivate Producuct
    module.exports.deactivateProduct = (id) => {
        let updates = {
            isActive: false
        }
        return Product.findByIdAndUpdate(id, updates).then((archived, error) => {
            if (archived) {
                return true;
            }else{
                return false;
            };
        });
    };
// Reactivate Product
    module.exports.reactivateProduct = (id) => {
        let updates = {
            isActive: true
        }
        return Product.findByIdAndUpdate(id, updates).then((reactivate, error) => {
            if (reactivate) {
                return false;
            }else{
                return true;
            };
        });
    };

    // [SECTION] Functionality [DELETE]
    module.exports.deleteProduct = (id) => {
        return Product.findByIdAndRemove(id).then((removeProduct, err) => {
        if (err) {
            return false;
        }else{
            return true;
        }
        });

    };

 